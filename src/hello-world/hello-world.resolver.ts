import { Args, Float, Int, Query, Resolver } from '@nestjs/graphql';

@Resolver()
export class HelloWorldResolver {
  @Query(() => String, { description: 'Retorna hola mundo', name: 'hello' })
  helloWorld(): string {
    return 'Hola Mundo';
  }

  @Query(() => Float, { name: 'ramdomNumber' })
  getRandomNumber(): Number {
    return Math.random() * 100;
  }

  @Query(() => Int, { name: 'randomFromZeroTo' })
  getRandomFromZeroTo(
    @Args('to', { type: () => Int, nullable: true }) toNumber: number = 10,
  ): number {
    return Math.floor(Math.random() * toNumber);
  }
}
