import { Injectable, NotFoundException } from '@nestjs/common';
import { Todo } from './entity/todo.entity';
import { CreateTodoInput } from './dto/input/create.todo.input';
import { UpdateTodoInput } from './dto/input/update.todo.input';
import { StatusArgs } from './dto/args/status.args';

@Injectable()
export class TodoService {
  private todos: Todo[] = [
    { id: 1, description: 'Piedra del Alma', done: false },
    { id: 2, description: 'Piedra del Espacio', done: true },
    { id: 3, description: 'Piedra del Poder', done: false },
  ];

  get totalTodos() {
    return this.todos.length;
  }

  get pedingTodos() {
    return this.findAll({ status: false }).length;
  }

  get completedTodos() {
    return this.findAll({ status: true }).length;
  }

  findAll(statusArgs: StatusArgs): Todo[] {
    if (statusArgs.status !== undefined) {
      return this.todos.filter((todo) => todo.done === statusArgs.status);
    }
    return this.todos;
  }

  findOne(id: number): Todo {
    const todo = this.todos.find((todo) => todo.id === id);
    if (!todo) throw new NotFoundException(`Todo with id ${id} not found`);
    return todo;
  }

  create(createTodoInput: CreateTodoInput): Todo {
    const todo = new Todo();
    todo.description = createTodoInput.description;
    todo.id = Math.max(...this.todos.map((todo) => todo.id), 0) + 1;
    this.todos.push(todo);
    return todo;
  }

  update(updateTodoInput: UpdateTodoInput) {
    const todoToUpdate = this.findOne(updateTodoInput.id);

    if (updateTodoInput.description)
      todoToUpdate.description = updateTodoInput.description;
    if (updateTodoInput.done !== undefined)
      todoToUpdate.done = updateTodoInput.done;

    this.todos = this.todos.map((todo) => {
      return todo.id === updateTodoInput.id ? todoToUpdate : todo;
    });

    return todoToUpdate;
  }

  delete(id: number) {
    const todo = this.findOne(id);
    this.todos = this.todos.filter((todo) => todo.id !== id);
    return true;
  }
}
