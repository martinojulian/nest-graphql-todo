import { Field, ObjectType, Int } from '@nestjs/graphql';

@ObjectType()
export class AggregationsType {
  @Field(() => Int)
  total: number;

  @Field(() => Int)
  pending: number;

  @Field(() => Int, { deprecationReason: 'Comentario que usar' })
  completed: number;
}
