import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Todo } from './entity/todo.entity';
import { TodoService } from './todo.service';
import { CreateTodoInput } from './dto/input/create.todo.input';
import { UpdateTodoInput } from './dto/input/update.todo.input';
import { StatusArgs } from './dto/args/status.args';
import { AggregationsType } from './types/aggregations.type';

@Resolver(() => Todo)
export class TodoResolver {
  constructor(private readonly todoService: TodoService) {}

  @Query(() => [Todo], { name: 'todos' })
  findAll(@Args() statusArgs: StatusArgs) {
    return this.todoService.findAll(statusArgs);
  }

  @Query(() => Todo, { name: 'todo' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.todoService.findOne(id);
  }

  @Mutation(() => Todo, { name: 'createTodo' })
  createTodo(@Args('createTodoInput') createTodoInput: CreateTodoInput) {
    console.log({ createTodoInput });
    return this.todoService.create(createTodoInput);
  }

  @Mutation(() => Todo, { name: 'updateTodo' })
  updateTodo(@Args('updateTodoInput') updateTodoInput: UpdateTodoInput) {
    return this.todoService.update(updateTodoInput);
  }

  @Mutation(() => Boolean)
  removeTodo(@Args('id', { type: () => Int }) id: number) {
    return this.todoService.delete(id);
  }

  @Query(() => Int)
  totalTodos() {
    return this.todoService.totalTodos;
  }
  @Query(() => Int)
  pendingTodos() {
    return this.todoService.pedingTodos;
  }

  @Query(() => Int)
  completedTodos() {
    return this.todoService.completedTodos;
  }

  @Query(() => AggregationsType)
  aggregation(): AggregationsType {
    return {
      completed: this.todoService.completedTodos,
      pending: this.todoService.pedingTodos,
      total: this.todoService.totalTodos,
    };
  }
}
